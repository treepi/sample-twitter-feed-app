//
//  ViewController.swift
//  SampleTwitterFeedApp
//
//  Created by Arash Sadeghieh E on 13/04/2016.
//  Copyright © 2016 Arash Sadeghieh Eshtehadi. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    @IBOutlet weak var userScreenNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showTweets" {
            let tweetVC = segue.destinationViewController as! TweetsViewController
            tweetVC.screenName = userScreenNameTextField.text!
        }
    }

    @IBAction func checkTweetsButtonTapped(sender: UIButton) {
        //Alert user is textField is empty or white spaces
        let text = userScreenNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) ?? ""
        if !text.isEmpty {
            performSegueWithIdentifier("showTweets", sender: nil)
        }
        else {
            alert()
        }
    }
    
    // MARK: - Alert
    func alert() {
        let alert = UIAlertController(title: "Missing Screen Name!", message: "Please enter the screen name.", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }

}

