//
//  TweetsViewController.swift
//  SampleTwitterFeedApp
//
//  Created by Arash Sadeghieh E on 14/04/2016.
//  Copyright © 2016 Arash Sadeghieh Eshtehadi. All rights reserved.
//

import UIKit

class TweetsViewController: UIViewController, UITableViewDataSource, TwitterTweetDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var serviceWrapper: TwitterServiceWrapper = TwitterServiceWrapper()
    var tweets = [TwitterTweet]()
    var screenName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        serviceWrapper.delegate = self
        // Display the 20 tweets for the screenName
        serviceWrapper.getResponseForRequest("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=\(screenName)&count=20")

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweets.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("tweetCell", forIndexPath: indexPath) as! TweetTableViewCell
        
        let tweet = tweets[indexPath.row] as TwitterTweet
        cell.tweetLabel.text = tweet.tweetText
        
        return cell
    }
    
    // MARK: - TwitterFollowerDelegate methods
    
    func finishedDownloading(tweet: TwitterTweet) {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tweets.append(tweet)
            self.tableView.reloadData()
        })
    }

}
