//
//  TwitterScreenName.swift
//  SampleTwitterFeedApp
//
//  Created by Arash Sadeghieh E on 14/04/2016.
//  Copyright © 2016 Arash Sadeghieh Eshtehadi. All rights reserved.
//

import Foundation

import Foundation

struct TwitterTweet {
    var tweetText: String?
    
    init (tweetText: String) {
        
        self.tweetText = tweetText
        
    }
}